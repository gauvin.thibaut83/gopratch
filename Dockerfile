#
# Builder
#
FROM  golang:1.14.3-alpine as builder

ENV WORKDIR=/go/src/gitlab.com/gauvin.thibaut83/gopratch
RUN mkdir -p $WORKDIR
WORKDIR $WORKDIR

COPY . .

RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -o /tmp/gobin ./src

#
# Final
#
FROM alpine:3.11

COPY --from=builder /tmp/gobin /usr/local/bin/gopratch

ENTRYPOINT ["gopratch"]
