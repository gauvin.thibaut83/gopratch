GO_INPUT=./src
GO_OUTPUT=./tmp/gobin

## ----------------------
## Available make targets
## ----------------------
##

all: help
help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_-.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

run: ## Run app locally
	go run ${GO_INPUT} run -dm

build: ## Build app from sources
	@GOOS=linux GOARCH=amd64 go build -o ${GO_OUTPUT} ${GO_INPUT}

lint.install: ## Install linter
	go install github.com/golangci/golangci-lint/cmd/golangci-lint

lint: ## Lint sources code
	golangci-lint run -v

lint.fix: ## Lint && Fix sources code
	golangci-lint run --fix -v

test:
	go test -v ./src -coverprofile tmp/coverage.output
