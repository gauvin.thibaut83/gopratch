module gitlab.com/gauvin.thibaut83/gopratch

require (
	github.com/golangci/golangci-lint v1.27.0 // indirect
	github.com/jawher/mow.cli v1.1.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.5.1
)

go 1.14
