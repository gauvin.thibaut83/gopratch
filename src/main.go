package main

import (
	"os"

	cli "github.com/jawher/mow.cli"
	log "github.com/sirupsen/logrus"
)

const gopratchVersion = "v0.1"

func main() {
	app := cli.App("gopratch", "CLI to retrieve infos about your pending Pull-Requests")

	app.Command("run", "Start to scrap your pending Pull-Requests", cmdRunHandler)
	app.Command("-v --version version", "Print version information and quit", cmdGetVersionHandler)

	if err := app.Run(os.Args); err != nil {
		log.Errorf("%v", err)
		cli.Exit(1)
	}
}
