package main

type UserMergeRequests []MergeRequest

type MergeRequest struct {
	ID                        int      `json:"id"`
	Iid                       int      `json:"iid"`
	ProjectID                 int      `json:"project_id"`
	Title                     string   `json:"title"`
	Description               string   `json:"description"`
	State                     string   `json:"state"`
	CreatedAt                 string   `json:"created_at"`
	UpdatedAt                 string   `json:"updated_at"`
	MergedBy                  string   `json:"merged_by"`
	MergedAt                  string   `json:"merged_at"`
	ClosedBy                  string   `json:"closed_by"`
	ClosedAt                  string   `json:"closed_at"`
	TargetBranch              string   `json:"target_branch"`
	SourceBranch              string   `json:"source_branch"`
	UserNotesCount            int      `json:"user_notes_count"`
	UpVotes                   int      `json:"upvotes"`
	DownVotes                 int      `json:"downvotes"`
	SourceProjectID           int      `json:"source_project_id"`
	TargetProjectID           int      `json:"target_project_id"`
	Labels                    []string `json:"labels"`
	WorkInProgress            bool     `json:"work_in_progress"`
	Milestone                 string   `json:"milestone"`
	MergeWhenPipelineSucceeds bool     `json:"merge_when_pipeline_succeeds"`
	MergeStatus               string   `json:"merge_status"`
	MergeCommitSha            string   `json:"merge_commit_sha"`
	SquashCommitSha           string   `json:"squash_commit_sha"`
	DiscussionLocked          bool     `json:"discussion_locked"`
	ShouldRemoveSourceBranch  bool     `json:"should_remove_source_branch"`
	ForceRemoveSourceBranch   bool     `json:"force_remove_source_branch"`
	WebURL                    string   `json:"web_url"`
	Squash                    bool     `json:"squash"`
	TaskCompletionStatus      struct {
		Count          int `json:"count"`
		CompletedCount int `json:"completed_count"`
	}
	HasConflicts                bool   `json:"has_conflicts"`
	BlockingDiscussionsResolved bool   `json:"blocking_discussions_resolved"`
	ApprovalsBeforeMerge        string `json:"approvals_before_merge"`
}
