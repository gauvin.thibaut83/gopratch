package main

import (
	"errors"
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type SecretsError struct {
	Err error
}

func (r *SecretsError) Error() string {
	return fmt.Sprintf("err %v", r.Err)
}

func loadEnvSecrets() (Secrets, error) {
	Secrets := Secrets{}
	envconfig.MustProcess("gitlab", &Secrets)

	if Secrets.APIBaseURL == "" || Secrets.UserToken == "" {
		return Secrets, &SecretsError{errors.New("you should set both 'GITLAB_API_BASE_URL' and 'GITLAB_USER_TOKEN' env vars to properly use gopratch")}
	}

	return Secrets, nil
}
