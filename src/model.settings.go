package main

type Settings struct {
	Secrets
	Config
}

type Config struct {
	Debug bool
	Mock  bool
	Wip   bool
}

type Secrets struct {
	APIBaseURL string `envconfig:"API_BASE_URL"`
	UserToken  string `envconfig:"USER_TOKEN"`
}
