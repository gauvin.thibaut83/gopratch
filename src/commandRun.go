package main

import (
	cli "github.com/jawher/mow.cli"
	log "github.com/sirupsen/logrus"
)

func cmdRunHandler(cmd *cli.Cmd) {
	config := Config{}
	cmd.BoolOptPtr(&config.Debug, "d debug", false, "Debug mode, logs are more verbose")
	cmd.BoolOptPtr(&config.Mock, "m mock", false, "Return mocked data instead of real response")
	cmd.BoolOptPtr(&config.Wip, "w wip", false, "Also display \"work in progress\" Pull-Requests")

	cmd.Action = func() {
		if err := cmdRunExec(config); err != nil {
			log.Fatalf("An error occurred: %v", err)
			cli.Exit(1)
		}
	}
}

func cmdRunExec(config Config) error {
	if config.Debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("Debug mode activated !")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	secret, err := loadEnvSecrets()
	if err != nil {
		return err
	}

	settings := Settings{secret, config}
	log.Debugf("Settings: %v", settings)
	log.Debug("=== Scrapping Pull-Requests of given user ===")

	userMergeRequests, err := getUsersMergeRequests(settings)
	if err != nil {
		return err
	}

	displayMergeRequests(userMergeRequests, settings.Config.Wip)
	log.Debug("=== Done ===")

	return nil
}
