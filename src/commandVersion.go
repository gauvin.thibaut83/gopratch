package main

import (
	"fmt"

	cli "github.com/jawher/mow.cli"
)

func cmdGetVersionHandler(cmd *cli.Cmd) {
	cmd.Action = func() {
		fmt.Printf("%s\n", gopratchVersion)
	}
}
