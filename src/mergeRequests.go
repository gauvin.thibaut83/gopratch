package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	timeout = 5 * time.Second
)

func getUsersMergeRequests(settings Settings) (UserMergeRequests, error) {
	if settings.Config.Mock {
		log.Debug("return mocked response")
		mockedUserMergeRequests, err := getMockedMergeRequests()

		return mockedUserMergeRequests, err
	}

	url := fmt.Sprintf("%s/api/v4/merge_requests?state=opened&scope=created_by_me", settings.Secrets.APIBaseURL)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("PRIVATE-TOKEN", settings.Secrets.UserToken)
	httpClient := http.Client{Timeout: timeout}

	response, err := httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	log.Debugf("%+v", response)

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	userMergeRequests := UserMergeRequests{}

	err = json.Unmarshal(body, &userMergeRequests)
	if err != nil {
		return nil, err
	}

	return userMergeRequests, nil
}

func getMockedMergeRequests() (UserMergeRequests, error) {
	filename := "tests/users_merge_requests.json"

	source, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	userMergeRequests := UserMergeRequests{}

	err = json.Unmarshal(source, &userMergeRequests)
	if err != nil {
		return nil, err
	}

	return userMergeRequests, nil
}

func displayMergeRequests(userMergeRequests UserMergeRequests, displayWip bool) {
	for _, mergeRequest := range userMergeRequests {
		if !displayWip && mergeRequest.WorkInProgress {
			continue
		}

		log.Debugf("%v", mergeRequest)

		log.Infof("=== [%s] ===", mergeRequest.WebURL)
		log.Infof("tile: %s", mergeRequest.Title)
		log.Infof("%s ~> %s", mergeRequest.SourceBranch, mergeRequest.TargetBranch)
		log.Infof("notes: %d", mergeRequest.UserNotesCount)
		log.Infof("wip: %t", mergeRequest.WorkInProgress)
		log.Infof("status: %s", mergeRequest.MergeStatus)
	}
}
