package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func setup() {
	_ = os.Setenv("GITLAB_API_BASE_URL", "https://localhost")
	_ = os.Setenv("GITLAB_USER_TOKEN", "test_token")
}

func clean() {
	_ = os.Unsetenv("GITLAB_API_BASE_URL")
	_ = os.Unsetenv("GITLAB_USER_TOKEN")
}

func TestLoadEnvSecretsSuccess(t *testing.T) {
	setup()

	Secrets, err := loadEnvSecrets()
	assert.NoErrorf(t, err, "blabla")

	assert.Equal(t, Secrets.APIBaseURL, "https://localhost")
	assert.Equal(t, Secrets.UserToken, "test_token")

	clean()
}

func TestLoadEnvSecretsErrors(t *testing.T) {
	clean()

	_, err := loadEnvSecrets()
	assert.Error(t, err)
}
