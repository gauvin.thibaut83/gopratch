[![coverage report](https://gitlab.com/gauvin.thibaut83/gopratch/badges/master/coverage.svg)](https://gitlab.com/gauvin.thibaut83/gopratch/commits/master)
[![pipeline status](https://gitlab.com/gauvin.thibaut83/gopratch/badges/master/pipeline.svg)](https://gitlab.com/gauvin.thibaut83/gopratch/commits/master)

### GoPratch

Personal pull-request watcher, in command line interface written in Golang

#### Usage

- With docker:
```bash
docker run -ti --rm \
    -e GITLAB_API_BASE_URL="https://gitlab.example.com" \
    -e GITLAB_USER_TOKEN="your_access_token" \
    registry.gitlab.com/gauvin.thibaut83/gopratch/release:v0.1 \
    sh
```
